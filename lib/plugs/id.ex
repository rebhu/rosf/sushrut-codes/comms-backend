defmodule CommsWeb.Plugs.ID do
  import Plug.Conn

  def init(default), do: default

  def call(conn, _default) do
    conn
    |> get_req_header("custom-id")
    |> IO.inspect()

    conn
    |> check_id()
  end

  defp check_id(conn) do
    conn
    |> get_req_header("custom-id")
    |> List.first()
    |> UUID.info()
    |> case do
      {:ok, _} ->
        conn

      {:error, _} ->
        id = UUID.uuid4()
        conn
        |> put_resp_header("custom-id", id)
    end
  end
end
