defmodule CommsWeb.DataController do
  use CommsWeb, :controller
  alias Comms.Data

  def headers(conn, _params) do
    IO.inspect conn
    conn
    |> send_resp(200, "OK")
  end

  def index(conn, params) do
    docs = Data.list(params)

    conn
    |> put_resp_header("X-CSRF-Token", get_csrf_token())
    |> render("index.json", datum: docs)
  end

  # def show(conn, params) do
  # end

  def create(conn, params) do
    id = conn |> get_req_header("custom-id")

    with {:ok, doc} <-
           params
           |> Map.put("creator_id", id |> List.first())
           |> Map.put("type", "data")
           |> Data.create() do
      conn
      |> render("show.json", data: doc |> Map.from_struct() |> Map.fetch!(:fields))
    end
  end

  # def update(conn, params) do
  # end

  # def delete(conn, params) do
  # end

end
