defmodule CommsWeb.EndorsementController do
  use CommsWeb, :controller
  alias Comms.Data

  def index(conn, params) do
    with doc when is_map(doc) <-
           params
           |> Data.list_endorsement() do
      conn
      |> render("show.json", endorsement: doc)
    end
  end

  def show(conn, params) do
    creator_id = params["id"]

    with [doc] <-
           params
           |> Map.drop(["id"])
           |> Map.put("creator_id", creator_id)
           |> Data.show_endorsement() do
      IO.inspect(doc)

      conn
      |> render("show.json", endorsement: doc)
    end
  end

  def create(conn, params) do
    creator_id = conn |> get_req_header("custom-id")

    with true <- params["data_id"] != "custom",
           {:ok, doc} <- params
         |> Map.put("creator_id", creator_id |> List.first())
         |> Map.put("type", "endorsements")
         |> Data.create_endorsement() do

      conn
      |> render("show.json", endorsement: doc |> Map.from_struct() |> Map.get(:fields))
    end
  end

  # def update(conn, params) do
  # end

  # def delete(conn, params) do
  # end
end
