# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :comms_backend,
  namespace: Comms

# Configures the endpoint
config :comms_backend, CommsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "C6Y7xXDmXOc0K0hlTUBoZhdOxmU0pzHZlCWC2Nb4fbjSg/JbYAA2oA3ZfSHXgtyB",
  render_errors: [view: CommsWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Comms.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "TPTEEeHH"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :comms_backend, Comms.Relax,
  couchdb_ip: System.get_env("COUCHDB_IP"),
  couchdb_port: System.get_env("COUCHDB_PORT"),
  couchdb_username: System.get_env("COUCHDB_USERNAME"),
  couchdb_password: System.get_env("COUCHDB_PASSWORD")
